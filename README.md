Fitness Monitor
=============

Current build: 1.0.0.1

Introduction
-------------
The body mass index (BMI), or Quetelet index, is a measure for human body shape
based on an individual's weight and height. It was devised between 1830 and 1850
by the Belgian polymath Adolphe Quetelet during the course of developing "social
physics". Body mass index is defined as the individual's body mass divided by 
the square of their height. The formulae universally used in medicine produce a
unit of measure of kg/m^2. BMI can also be determined using a BMI chart, which
displays BMI as a function of weight (horizontal axis) and height (vertical 
axis) using contour lines for different values of BMI or colors for different 
BMI categories.

Formula
-------

BMI = mass(kg) / height(m)^2 = (mass(lb) / height(in)^2) * 703


Revisions
=========

Version 1.0.0.1
-------------

Bug fixes

Version 1.0.0.0
-------------

Initial release


License notes
=============

Graphic assets
--------------
The graphic assets are not free and therefore it is not allowed to be 
downloaded, copied, redistributed or used in any other way.
